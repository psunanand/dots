#!/usr/bin/env bash

# Inspired by https://github.com/AngellusMortis/arch-installer

set -o errexit # exit on errors
set -o errtrace # Make sure any error trap is inherited
set -o nounset # Disallow expansion of unset variables
set -o pipefail # Use last non-zero exit code in a pipeline

main(){
    source "$(dirname "${BASH_SOURCE[0]}")/libs/util.sh"
    source "$(dirname "${BASH_SOURCE[0]}")/libs/base.sh"

    script_init "$@"
    init_params

    trap script_trap_err ERR
    trap script_trap_exit EXIT

    parse_params "$@"
    script_log

    run "Configure" "get_params $script_params"
    run "Variables" "print_variables"

    if [[ "$dry_run" = false ]]; then
        run "Syncing system time" "timedatectl set-ntp true"
        run "Cleaning disk" "erase_disk"
      if [[ "$do_wipe" = true ]]; then
        run "Securely wiping disk" "securely_wipe_disk"
      fi
      run "Partitioning disk" "partition_disk"
      run "Bootstrapping arch" "bootstrap_arch"
      run "Chroot-installing" "do_chroot"
      run "Rebooting" "shutdown -r now"
    fi
}

main "$@"
