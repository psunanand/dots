#!/usr/bin/env bash

tmpimage=$(mktemp --suffix=.png)

# take a full screenshot
import -window root "$tmpimage"

# pixelate the screenshot by resizing and scaling up
convert "$tmpimage" -scale 10% -scale 1000% "$tmpimage"

# run i3lock with custom background
i3lock -u -i "$tmpimage"
