#!/usr/bin/env bash

case $1 in
  laptop)
    xrandr --output DP1 --off --output eDP1 --primary --mode 1920x1080 \
      --pos 0x0 --rotate normal --output DP2 --off --output HDMI1 --off \
      --output VIRTUAL1 --off
    ;;
  monitor)
    xrandr --output eDP1 --off --output DP1 --primary --mode 2560x1440 \
      --pos 0x0 --rotate normal --output DP2 --off --output HDMI1 --off \
      --output VIRTUAL1 --off
    ;;
  dual)
    xrandr --output eDP1 --primary --mode 1920x1080 --pos 2560x0 \
      --rotate normal --output DP1 --mode 2560x1440 --pos 0x0 \
      --rotate normal --output DP2 --off --output HDMI1 --off \
      --output VIRTUAL1 --off
    ;;
  *)
    echo "Usage: $0 {laptop|monitor|dual}"
    exit 2
esac
