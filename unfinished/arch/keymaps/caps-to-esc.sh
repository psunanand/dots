#!/usr/bin/env bash

# Switch CapsLock to ESC/Ctrl and ESC to CapsLock

set -e

sudo pacman -S interception-caps2esc

sudo tee /etc/udevmon.yaml >/dev/null <<-EOF
- JOB: "intercept -g \$DEVNODE | caps2esc | uinput -d \$DEVNODE"
  DEVICE:
      EVENTS:
            EV_KEY: [KEY_CAPSLOCK, KEY_ESC]
EOF

sudo tee /etc/systemd/system/udevmon.service >/dev/null <<-EOF
    [Unit]
    Description=udevmon
    Wants=systemd-udev-settle.service
    After=systemd-udev-settle.service
    [Service]
    ExecStart=/usr/bin/nice -n -20 /usr/bin/udevmon -c /etc/udevmon.yaml
    [Install]
    WantedBy=multi-user.target
EOF

sudo systemctl enable --now udevmon
