#!/usr/bin/env bash

# Add Thai Kinnari font and TH keyboard  after stowed
# Check X11 Model <pc105> using localectl

set -e

fc-cache -f
is_found=$(fc-list | grep -i 'kinnari')

[[ -n "$is_found" ]] \
  && sudo localectl set-x11-keymap "us,th" pc105 "" grp:alt_shift_toggle \
  || exit 1

printf '%s\n' 'TH language+keyboard has been added!'
