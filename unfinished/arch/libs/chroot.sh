#!/usr/bin/env bash

set -o errexit # exit on errors
set -o errtrace # Make sure any error trap is inherited
set -o nounset # Disallow expansion of unset variables
set -o pipefail # Use last non-zero exit code in a pipeline

usage() {
  cat << EOF
Usage:

  Simple program that sets up the system after arch-chroot

  -h|--help      Print usage info and exit

Argument(s):
  -n|--hostname  Hostname of the OS
  -v|--device    Harddisk to install Arch Linux - "$(block_devices)"

Option(s):
  -p|--pause     Pause after each installation step (default=true)
  -e|--efi       Use UEFI instead of BIOS to boot (default=true)
  -y|--encrypt   Encrypt disk (default=false)
EOF
}

init_params() {
  hostname="mk-arch"
  do_efi=true
  do_pause=true
  do_encrypt=true
  device="/dev/nvme0n1"
}

parse_params() {
  local param
  while [[ $# -gt 0 ]]; do
    param="$1"
    case $param in
      -h|--help)
        shift
        usage
        exit 0
        ;;
      -p|--pause)
        shift
        do_pause=true
        ;;
      -n|--hostname)
        shift
        hostname=$1
        shift
        ;;
      -y|--encrypt)
        shift
        do_encrypt=true
        ;;
      -e|--efi)
        shift
        do_efi=true
        ;;
      -v|--device)
        shift
        device=$1
        shift
        ;;
      -*|--*|*)
        echo "Invalid param was provided: $param"
        exit 1
        ;;
    esac
  done
}

set_locales() {
  timedatectl set-timezone "Asia/Bangkok"
  hwclock --systohc
  sed --in-place=.bak 's/^#en_US\.UTF-8/en_US\.UTF-8/' /etc/locale.gen
  locale-gen
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
}

set_host() {
  echo $hostname > /etc/hostname
}

install_bootloader() {
  if [ "$do_encrypt" = true ]; then
    pacman -S lvm2 grub --noconfirm

    sed --in-place=.bak 's/HOOKS=.*/HOOKS=\(base udev autodetect keyboard keymap modconf resume block encrypt lvm2 filesystems fsck\)/' /etc/mkinitcpio.conf
    mkinitcpio -p linux

    sed "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=${device}3:cryptlvm cryptkey=\/dev\/OS\/swap\"/" /etc/default/grub
  else
    pacman -S grub --noconfirm
  fi

  if [ "$do_efi" = true ]; then
    pacman -S efibootmgr --noconfirm
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --recheck
    grub-mkconfig -o /boot/grub/grub.cfg
  else
    grub-install $device
    grub-mkconfig -o /boot/grub/grub.cfg
  fi
}

set_root(){
  passwd
}

set_user(){
  read -r -p "Enter a new username: " username
  useradd -m -g wheel -s /bin/bash $username
  passwd $username
  sed --in-place=.bak 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
}

install_core_package(){
  pacman -S networkmanager git neovim man man-pages tldr python --noconfirm
}

install_packages(){
  /root/arch/libs/packages.sh
}

main() {
  source "$(dirname "${BASH_SOURCE[0]}")/util.sh"

  script_init "$@"
  init_params

  trap script_trap_err ERR
  trap script_trap_exit EXIT

  parse_params "$@"
  script_log

  run "Setting locales" "set_locales"
  run "Setting hostname" "set_host"
  run "Setting root" "set_root"
  run "Setting user" "set_user"
  run "Installing bootloader" "install_bootloader"
  run "Installing core packages" "install_core_package"
  run "Enabling core services" "systemctl enable NetworkManager.service"
  run "Installing packages" "install_packages"
}

main "$@"
