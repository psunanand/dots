#!/usr/bin/env bash

set -o errexit # Exit on errors
set -o errtrace # Make sure any error trap is inherited
set -o nounset # Disallow expansion of unset variables
set -o pipefail # Use last non-zero exit code in a pipeline

install_from_aur(){
  curl -O "https://aur.archlinux.org/cgit/aur.git/snapshot/$1.tar.gz" \
    && tar -xvf "$1.tar.gz" \
    && cd "$1" \
    && makepkg --noconfirm -si \
    && cd - \
    && rm -rf "$1" "$1.tar.gz"
}

install_from_pacman(){
  while read -r line
  do
    pacman --noconfirm --needed -S $line
  done < $(dirname ${BASH_SOURCE[0]})/db_packages.txt
}

install_from_yay(){
  while read -r line
  do
    yay --noconfirm -S $line
  done < $(dirname ${BASH_SOURCE[0]})/nondb_packages.txt
}

main(){
  source "$(dirname "${BASH_SOURCE[0]}")/util.sh"
  script_init "$@"

  trap script_trap_err ERR
  trap script_trap_err EXIT

  script_log

  run "Installing yay" "install_from_aur yay"
  run "Installing packages from official database" "install_from_pacman"
  run "Installing packages from non-official database" "install_from_yay"
}

main "$@"
