usage(){
  cat << EOF
Usage:

  Simple program that installs Arch Linux

  -h|--help      Print usage info and exit

Argument(s):
  -n|--hostname  Hostname of the OS
  -s|--swap-size Swap size in GB
  -v|--device    Harddisk to install Arch Linux - "$(block_devices)"

Option(s):
  -p|--pause     Pause after each installation step (default=true)
  -w|--wipe      Securely wipe the disk before partitioning (default=false)
  -e|--efi       Use UEFI instead of BIOS to boot (default=true)
  -y|--encrypt   Encrypt disk (default=false)
  -c|--clean     Clean up disks for compaction after installing (default=false)
  -d|--dry-run   Display final configuraitons without proceeding (default=false)
EOF
}

parse_params(){
  local param
  while [[ "$#" -gt 0 ]]; do
    param="$1"
    case "$param" in
      -h|--help)
        usage
        exit 0
        ;;
      -n|--hostname)
        shift
        hostname="$1"
        shift
        ;;
      -v|--device)
        shift
        device="$1"
        shift
        ;;
      -s|--swap-size)
        shift
        swap_size="$1"
        shift
        ;;
      -p|--pause)
        shift
        do_pause=true
        ;;
      -w|--wipe)
        shift
        do_wipe=true
        ;;
      -e|--efi)
        shift
        do_efi=true
        ;;
      -y|--encrypt)
        shift
        do_encrypt=true
        ;;
      -c|--clean)
        shift
        do_cleanup=true
        ;;
      -d|--dry-run)
        shift
        dry_run=true
        ;;
      -*|--*|*)
        echo "Invalid param was provided: $param"
        exit 1
        ;;
    esac
  done
}

init_params() {
  hostname="mk-arch"
  swap_size=16
  device="/dev/nvme0n1"
  do_pause=true
  do_wipe=false
  do_efi=true
  do_encrypt=false
  do_cleanup=false
  dry_run=false
}

erase_disk(){
  swapoff -a
  wipefs -a "$device"
  dd if=/dev/zero of="$device" bs=512 count=1 conv=notrunc
}

securely_wipe_disk(){
  cryptsetup open --type plain -d /dev/urandom "$device" to_be_wiped
  dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress || true
  cryptsetup close to_be_wiped
}

partition_disk(){
  boot_partition="${device}1"
  swap_partition="${device}2"
  root_partition="${device}3"
  encrypt_partition=""

  [[ $"do_efi" = true ]] && boot_hex="ef00" || boot_hex="ef02"
  [[ $"do_encrypt" = true ]] && root_hex="8309" || root_hex="8304"

  partition_commands="
n
1

+1024M
${boot_hex}
n
2

+${swap_size}G
8200
n
3


${root_hex}
w
y
"
  echo "$partition_commands" | gdisk $device

  if [ "$do_encrypt" = true ]; then
    setup_encrypt
  fi

  mkswap $swap_partition
  swapon $swap_partition

  mkfs.ext4 $root_partition
  mount $root_partition /mnt
  if [[ "$do_efi" = true ]]; then
    mkfs.fat -F32 "${device}1"
    mkdir -p /mnt/boot/efi
    mount "${device}1" /mnt/boot/efi
  fi
}

setup_encrypt(){
  encrypt_partition=$root_partition
  root_partition=/dev/OS/root

  cryptsetup luksFormat --type luks1 $encrypt_partition
  cryptsetup open $encrypt_partition cryptlvm

  pvcreate /dev/mapper/cryptlvm
  vgcreate OS /dev/mapper/cryptlvm

  lvcreate -L ${swap_size}G OS -n swap
  swap_partition=/dev/OS/swap
  lvcreate -l 100%FREE OS -n root
}

get_params() {
  if [[ "$*" =~ "-v" ]]; then
    prompt_param "$device" "Disk to Install to?"
    device="$prompt_result"
  fi

  if [[ "$*" =~ "-w" ]]; then
    prompt_bool "$do_wipe" "Securely Wipe Disk"
    do_wipe=$prompt_result
  fi

  if [[ "$*" =~ "-y" ]]; then
    prompt_bool "$do_encrypt" "Encrypt Disk"
    do_encrypt=$prompt_result
  fi

  if [[ "$*" =~ "-n" ]]; then
    prompt_param "$hostname" "Hostname"
    hostname="$prompt_result"
  fi

  if [[ "$*" =~ "-s" ]]; then
    prompt_param "$swap_size" "Swap space to allocate in GB"
    swap="$prompt_result"
  fi

  if [[ "$*" =~ "-e" ]]; then
    prompt_bool "$do_efi" "Use EFI"
    do_efi=$prompt_result
  fi

  if [[ "$*" =~ "-c" ]]; then
    prompt_bool "$do_cleanup" "Clean up for disk compaction?"
    do_cleanup=$prompt_result
  fi
}

print_variables() {
  pretty_print "Hostname"
  pretty_print ": $hostname"

  pretty_print "Install Disk"
  pretty_print ": $device"

  pretty_print "Wipe Disk"
  pretty_print ": $do_wipe"

  pretty_print "Encrypt Disk"
  pretty_print ": $do_encrypt"

  pretty_print "Swap"
  pretty_print ": ${swap_size}GB"

  pretty_print "Using EFI"
  pretty_print ": $do_efi"

  pretty_print "Do Clean Up"
  pretty_print ": $do_cleanup"
}

bootstrap_arch() {
  pacstrap /mnt base base-devel linux linux-firmware
  genfstab -U /mnt >> /mnt/etc/fstab

  cp -R $script_dir /mnt/root/arch-installer
  chmod +x /mnt/root/arch-installer/script/chroot.sh
}

do_chroot(){
  extra_args=""

  if [ "$do_pause" = true ]; then
    extra_args="$extra_args -p"
  fi

  if [ "$do_encrypt" = true ]; then
    extra_args="$extra_args -y"
  fi

  if [ "$do_cleanup" = true ]; then
    extra_args="$extra_args -c"
  fi

  if [ "$do_efi" = true ]; then
    extra_args="$extra_args -e"
  fi

  arch-chroot /mnt /root/arch/libs/chroot.sh$extra_args -v $device -n $hostname

  rm -rf /mnt/root/arch
}
