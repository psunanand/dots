block_devices(){
  local devices="$(lsblk -d | grep -P "^(sd|hd|vd|nvme|mncblk)" \
    | awk '{print "/dev/" $1}' | paste -sd ' ')"
  echo $devices
}

script_trap_err() {
  local exit_code=1

  # Disable the error trap handler to prevent potential recursion
  trap - ERR

  # Consider any further errors non-fatal to ensure we run to completion
  set +o errexit
  set +o pipefail

  # Validate any provided exit code
  if [[ ${1:-} =~ ^[0-9]+$ ]]; then
    exit_code="$1"
  fi

  # Exit with failure status
  exit "$exit_code"
}

script_trap_exit() {
  cd "$cwd"

  if [[ -f ${script_output:-} ]]; then
    rm "$script_output"
  fi
}

script_exit() {
  if [[ $# -eq 1 ]]; then
    printf '%s\n' "$1"
    exit 0
  fi

  if [[ ${2:-} =~ ^[0-9]+$ ]]; then
    printf '%b\n' "$1"
    # If we've been provided a non-zero exit code run the error trap
    if [[ $2 -ne 0 ]]; then
      script_trap_err "$2"
    else
      exit 0
    fi
  fi

  script_exit 'Missing required argument to script_exit()!' 2
}

script_init(){
  readonly cwd="$PWD"
  readonly script_path="${BASH_SOURCE[1]}"
  readonly script_dir="$(dirname "$script_path")"
  readonly script_name="$(basename "$script_path")"
  readonly script_params="$*"

  prompt_results=""
}

script_log(){
  readonly script_output="$(mktemp --tmpdir "$script_name".XXXXX)"
  exec 3>&1 4>&2 1> "$script_name" 2>&1
  set -x
}

pretty_print() {
  if [[ $# -lt 1 ]]; then
    script_exit 'Missing required argument to pretty_print()!' 2
  fi

  if [[ -n ${2:-} ]]; then
    printf '%s' "$1"
  else
    printf '%s\n' "$1"
  fi
}

run(){
  pretty_print "Running ====> $1"
  $2
  echo

  if [[ "$do_pause" = true ]]; then
    pretty_print "|Paused|"
    read -n 1 -s
  fi
}

prompt_param() {
  local default="$1"
  local prompt="$2"
  local value

  pretty_print "$prompt (default: $default)?"
  read value

  prompt_result=${value:-$default}
}

prompt_bool() {
  local default=$1
  local prompt="$2"

  if [ "$default" = true ]; then
    default="yes"
  else
    default="no"
  fi

  prompt_result=""

  while true; do
    prompt_param "$default" "$prompt"
    case $prompt_result in
      [Yy]|[Yy][Ee][Ss] )
        prompt_result=true
        break
        ;;
      [Nn]|[Nn][Oo] )
        prompt_result=false
        break
        ;;
    esac
  done
}
