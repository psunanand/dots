stow:
	stow --verbose --restow --target=$$HOME stow-home

unstow:
	stow --verbose --delete --target=$$HOME stow-home
