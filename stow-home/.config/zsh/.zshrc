# Zshrc

# Paths {{{
typeset -U fpath=(
"${ZDOTDIR}/functions"
$fpath
)
# }}}

# Options {{{
# Directory
setopt AUTO_PARAM_SLASH  # Add suffix '/' if the name is a directory
unsetopt CASE_GLOB       # Make globbing case-insensitive
setopt AUTO_PUSHD        # Push visited directory onto the stack
setopt PUSHD_IGNORE_DUPS # Ignore duplicate directories in the stack
setopt PUSHD_SILENT      # Don't print when pushd or popd

# History
setopt APPEND_HISTORY          # Keep appending to the hist file
setopt EXTENDED_HISTORY        # Write to the hist file in :start:elapsed;command format
setopt HIST_EXPIRE_DUPS_FIRST  # Remove old duplicaties if the hist file is filled up
setopt HIST_FIND_NO_DUPS       # Don't display duplicates when searching
setopt HIST_IGNORE_DUPS        # Don't store repeated cmds adjacent to one another
setopt HIST_IGNORE_ALL_DUPS    # Don't store repeated cmds even if they're not adjacent to one another
setopt HIST_IGNORE_SPACE       # Don't keep any cmd if it starts with space
setopt HIST_SAVE_NO_DUPS       # Don't write duplicates to the hist file
setopt HIST_VERIFY             # Do history substitution before executing the cmd
setopt INC_APPEND_HISTORY      # Write to the hist file as typed
setopt SHARE_HISTORY           # Share the hist file across sessions

# I/O
unsetopt RM_STAR_SILENT # Confirm when rm with "*"
unsetopt BEEP # Turn off all beeps

# Jobs
setopt CHECK_JOBS # Report the status of bg jobs on 1st attempt to exit shell
setopt HUP        # Kill running jobs on shell exit
# }}}

# Autocompletion {{{
# Note that other auto-completion functions are already in $fpath after
# sudo pacman -S zsh-completions
autoload -Uz compinit; compinit
_comp_options+=(globdots)
completion_style="${ZDOTDIR}/completion.zsh"
[[ -f $completion_style ]] && source $completion_style
bindkey '^[[Z' reverse-menu-complete
# }}}

# Vi Mode {{{
# Activate Vi Mode
bindkey -v

# Add Vi flavor to the cursor
autoload -Uz vicursor; vicursor

# Add other cool Vi features
autoload -Uz select-quoted select-bracketed surround
zle -N select-quoted
zle -N select-bracketed
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround

for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $m $c select-bracketed
  done
done
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -M visual S add-surround

# Add latency for "surround"
export KEYTIMEOUT=20

# Restore some keymaps removed by Vi keybind mode
bindkey -M viins '^k' up-history
bindkey -M viins '^j' down-history
bindkey -M viins '^h' backward-delete-char
bindkey -M viins '^?' backward-delete-char
bindkey -M viins '^w' backward-kill-word

# Use Vi-like history search keys
autoload -Uz up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search

# Use Vi keys in tab complete menu
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Edit line in $EDITOR
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey '^x^e' edit-command-line
# }}}

# Theme {{{
theme="${ZDOTDIR}/simple_prompt.zsh"
[[ -f $theme ]] && source $theme
# }}}

# System clipboard integration {{{
system_clipboard=/usr/share/zsh/plugins/zsh-system-clipboard/zsh-system-clipboard.zsh
[[ -f $system_clipboard ]] && source $system_clipboard
# }}}

# Utility Functions {{{
# Jump to any level of parent directories
autoload -Uz bd; bd

# Ctrl-z to jump between fg and bg job
autoload -Uz fancy-ctrl-z; zle -N fancy-ctrl-z
bindkey '^z' fancy-ctrl-z

# Man with colors
autoload -Uz man

# Man with Vim
autoload -Uz vman

# Run programs in background
autoload -Uz background

# Expand aliases
autoload -Uz expand-alias; zle -N expand-alias
# Use <Space> to expand alias
bindkey -M viins " " expand-alias
# Use Ctrl-<Space> to bypass the alias expansion
bindkey -M viins "^ " magic-space
# Disable alias expansion during search
bindkey -M isearch " " magic-space
# }}}

# Aliases {{{

# required by expand-alias
typeset -a ealiases
ealiases=()

ealias(){
  alias "$@"
  args="$@"
  # no '=' and 'front empty space'
  args=${args%%\=*}
  ealiases+=(${args##* })
}

alias nvim="$VISUAL"

ealias d='dirs -v'
for index ({1..5}); do alias "$index"="cd +${index}"; done
unset index

if (( $+commands[exa] )); then
  ealias la='exa -la --group-directories-first'
  ealias ll='exa -l --group-directories-first'
  alias tree='exa --tree -I .git'
else
  alias la='ls -lah'
  alias ll='ls -lh'
fi

if (( $+commands[bat] )); then
  alias cat=bat;
fi

alias -s html='background firefox'
alias -s {pdf,PDF}='background zathura'
# }}}

# FZF {{{
if (( $+commands[fzf] )); then
  source ~/.fzf/shell/completion.zsh
  source ~/.fzf/shell/key-bindings.zsh
fi
# }}}

# Pyenv {{{
if (( $+commands[pyenv] )); then
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
  # ensure no path clobbing from those pyenv-init commands
  typeset -U path fpath
fi
# }}}

# Syntax Highlighting {{{
syntax_highlighting=~/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[[ -f $syntax_highlighting ]] && source $syntax_highlighting
# }}}

# i3 automatic startup {{{
# [[ "$(tty)" = "/dev/tty1" ]] && (pgrep "i3" || exec startx "$HOME/.xinitrc")
# }}}
