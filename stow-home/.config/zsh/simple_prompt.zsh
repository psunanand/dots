# Simple Prompt Configurations

# Set prompt character
typeset -A simple_symbol=(
    "user" ">>"
    "root" "##"
)

# Set vimode character
typeset -A simple_vimode=(
    "insert" ":"
    "normal" "!"
)

# Trim path longer than 'percent_length' of the monitor
typeset -A simple_directory=(
    "percent_length" 50
)

# Use Xterm 256 color codes
typeset -A simple_color=(
    # blue
    "default" 12
    # purple
    "info" 147
    # red
    "negative" 9
    # green
    "positive" 10
    # yellow
    "attention" 229
)

simple_concat() {
  local -a res=()
  local out=""
  for func in ${(P)1}; do
    out="$($func)"
    if [[ -n "$out" ]]; then
        res+="$out"
    fi
  done
  printf "%s" "${(j: :)res}"
}

simple_status() {
    local return_status_color="%(?.%F{${simple_color[default]}}.%F{${simple_color[negative]}})"
    local shlvl="[%L%(1j.:+%j.)]"
    local symbol="%(!.$simple_symbol[root].$simple_symbol[user])"
    local vimode="${${KEYMAP/vicmd/${simple_vimode[normal]}}/(main|viins)/${simple_vimode[insert]}}"
    printf "%b" "%B$return_status_color$shlvl$vimode$symbol%f%k%b"
}

simple_pwd() {
    local directory_path="%~"
    (( $((${#PWD} / $COLUMNS.0 * 100)) > ${simple_directory[percent_length]} )) && directory_path="../%3/"
    printf "%b" "%B%F{${simple_color[info]}}$directory_path%b%f"
}

simple_git() {
    local default_color="%F{$simple_color[positive]}"
    local branch_name="$(git rev-parse --abbrev-ref HEAD 2> /dev/null)"
    local branch_status="$(git status --porcelain 2> /dev/null)"
    local staged="$(grep -e '^[MADRCU]' <<< $branch_status 2> /dev/null)"
    local unstaged="$(grep -e '^[MADRCU? ][MADRCU?]' <<< $branch_status 2> /dev/null)"

    if [[ -n $staged ]]; then
        default_color="%F{$simple_color[negative]}"
    elif [[ -n $unstaged ]]; then
        default_color="%F{$simple_color[attention]}"
    fi

    printf "%b" "$default_color$branch_name%f"
}

simple_bind_widget() {
    eval "zle-keymap-select() {zle reset-prompt; $functions[zle-keymap-select]}"
    zle -N zle-keymap-select
}

setopt prompt_subst
setopt transient_rprompt

simple_prompt=(simple_status)
simple_rprompt=(simple_pwd simple_git)

PS1='$(simple_concat simple_prompt) '
RPS1='$(simple_concat simple_rprompt)'

simple_bind_widget
