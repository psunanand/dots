if exists("g:loaded_scratch")
  finish
endif
let g:loaded_scratch = 1

let s:save_cpo = &cpo
set cpo&vim

command! -nargs=* -complete=command Scratch call scratch#open('<mods>', <q-args>)

let &cpo = s:save_cpo
unlet s:save_cpo
