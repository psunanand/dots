if exists("g:loaded_common")
  finish
endif
let g:loaded_common = 1

let s:save_cpo = &cpo
set cpo&vim

command! RemoveTrailingSpace call common#preserve("%s/\\s\\+$//e")
command! ToggleZoom call common#toggle_zoom()
command! ToggleTooLongHighlight call common#too_long_highlight()
command! BufOnly silent! call common#preserve("%bdelete!\|edit#\|bdelete#")
command! W w !sudo tee % &>/dev/null
command! Cwd lcd %:p:h

xnoremap <silent> * :<C-u>call common#visual_search()<CR>/<C-R>=@/<CR><CR>
xnoremap <silent> # :<C-u>call common#visual_search()<CR>?<C-R>=@/<CR><CR>

augroup highlight_toolong
    autocmd!
    autocmd BufWinEnter,BufEnter * if &buftype !=# 'terminal' | call common#too_long_highlight()
augroup END

let &cpo = s:save_cpo
unlet s:save_cpo
