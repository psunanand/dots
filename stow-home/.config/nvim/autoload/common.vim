" Execute command while preserving the cursor position with untouched history
function! common#preserve(cmd)
  let _s = @/
  let l:view = winsaveview()
  execute a:cmd
  let @/ = _s
  call winrestview(l:view)
endfunction

" Toggle zoom
function! common#toggle_zoom() abort
  if exists('t:zoomed_winrestcmd') && (t:zoomed_winrestcmd.after == winrestcmd())
    execute t:zoomed_winrestcmd.before
    if t:zoomed_winrestcmd.before != winrestcmd()
      wincmd =
    endif
    unlet t:zoomed_winrestcmd
  else
    let t:zoomed_winrestcmd = {'before': winrestcmd()}
    resize | vertical resize
    let t:zoomed_winrestcmd['after'] = winrestcmd()
  endif
endfunction

" Allow to use * # to search for current selection
function! common#visual_search() abort
  let _s = @v
  normal! gv"vy
  let @/ = '\V' . substitute(escape(@v, '/\'), '\n', '\\n', 'g')
  let @v = _s
endfunction

" Toggle longline highlight
function! common#too_long_highlight() abort
  if exists('w:is_highlighted')
    silent! call matchdelete(w:is_highlighted)
    unlet w:is_highlighted
  else
    let w:is_highlighted = matchadd('ColorColumn', '\%>80v', 100)
  endif
endfunction
