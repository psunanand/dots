" Settings for help

augroup after_ft_help
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> nnoremap <buffer> q :quit<Cr>
augroup END
