" Settings for markdown

augroup after_ft_markdown
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> setlocal wrap textwidth=80 spell spelllang=en_us |
        \ setlocal makeprg=pandoc\ %\ -o\ %:p:r.pdf |
        \ nnoremap <silent> _z mm[s1z=`m
augroup END
