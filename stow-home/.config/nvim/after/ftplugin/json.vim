" Settings for json

" Get comment highlighting
augroup after_ft_json
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> syntax match Comment +\/\/.\+$+
augroup END
