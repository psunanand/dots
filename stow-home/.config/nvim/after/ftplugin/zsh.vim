" Settings for zsh

" Google's style conventions
augroup after_ft_zsh
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> setlocal shiftwidth=2 tabstop=2
        \ softtabstop=2 expandtab
augroup END
