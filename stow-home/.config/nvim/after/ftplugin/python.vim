" Settings for python

augroup after_ft_python
  autocmd! * <buffer>
  " PEP8's style conventions
  autocmd BufWinEnter <buffer> setlocal shiftwidth=4 tabstop=4 softtabstop=4
        \ expandtab autoindent smartindent
augroup END
