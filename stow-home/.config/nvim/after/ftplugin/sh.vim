" Settings for sh

" Google's style conventions
augroup after_ft_sh
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> setlocal shiftwidth=2 tabstop=2
        \ softtabstop=2 expandtab
augroup END
