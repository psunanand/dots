" Settings for vim

augroup after_ft_vim
  autocmd! * <buffer>
  " Google's style conventions
  autocmd BufWinEnter <buffer> setlocal shiftwidth=2 tabstop=2
        \ softtabstop=2 textwidth=80
augroup END
