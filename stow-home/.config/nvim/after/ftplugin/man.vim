" Settings for man

augroup after_ft_man
  autocmd! * <buffer>
  autocmd BufWinEnter <buffer> nnoremap <buffer> q :quit<Cr>
augroup END
