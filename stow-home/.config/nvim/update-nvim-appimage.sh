#!/usr/bin/env bash

# Get the latest stable version of Nvim appimage
# Nvim appimage will be located at /usr/bin/local/nvim
# Don't forget to alias it if you want to use this version

set -e

check_required_programs(){
  local required_progs=(curl xmllint)

  for prog in "${required_progs[@]}"; do
    if [[ ! -x $(command -v "$prog") ]]; then
      printf "%s\n" "${prog} is not installed! Aborting..."
      exit 1
    fi
  done
}

check_nvim_version_to_update(){
  local tmphtml=$(mktemp --suffix=.html)
  curl -sL -o "$tmphtml" "https://github.com/neovim/neovim/releases/tag/stable"
  local version_regex="v\d\.\d\.\d"
  # need to keep changing the xpath, since they change the layout from time to time
  local latest_nvim_version=$(xmllint --html --xpath "//code" "$tmphtml" 2>/dev/null | grep -oP "$version_regex")
  local current_nvim_version=""
  [[ -x /usr/local/bin/nvim ]] && \
    current_nvim_version=$(/usr/local/bin/nvim --version | head -n1 | grep -oP "$version_regex")
  if [[ "$latest_nvim_version" == "$current_nvim_version" ]]; then
    printf "%s\n" "Your Nvim stable is up-to-date with version '${current_nvim_version}'"
    exit 0
  elif [[ "$latest_nvim_version" != "$current_nvim_version" ]]; then
    printf "%s\n" "The latest Nvim stable version is ${latest_nvim_version}"
    prompt_update
  else
    printf "%s\n" "Cannot fetch the latest Nvim stable version from the repo! Aborting..."
    exit 1
  fi
}

prompt_update(){
  while read -p "Do you want to update Nvim stable to the latest version? [yes/no] " \
    && ! [[ $REPLY =~ [qQ] ]]; do
      case "$REPLY" in
        [yY]|[yY][eE][sS])
          update_nvim_appimage
          ;;
        [nN]|[nN][oO])
          exit 0
          ;;
        *)
          printf "%s\n" "Type [Yy]es or [Nn]o"
          ;;
      esac
  done
}

update_nvim_appimage(){
  local nvim_appimage_url="https://github.com/neovim/neovim/releases/download/stable/nvim.appimage"
  curl -L -o "/tmp/nvim" "$nvim_appimage_url"
  chmod u+x /tmp/nvim
  sudo mv /tmp/nvim /usr/local/bin/nvim
  command -v /usr/local/bin/nvim &> /dev/null
  case "$?" in
    0)
      printf "%s\n" "Your Nvim stable has been updated!"
      exit 0
      ;;
    *)
      printf "%s\n" "Fail to update Nvim stable"
      exit 1
      ;;
  esac
}

main(){
  check_required_programs
  check_nvim_version_to_update
}

main
