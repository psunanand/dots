# Initialize XDG variables to their default values if unset
: ${XDG_CACHE_HOME:="$HOME/.cache"}
: ${XDG_CONFIG_HOME:="$HOME/.config"}
: ${XDG_DATA_HOME:="$HOME/.local/share"}

# Create a runtime temporary directory with suffix to accommodate multiple users
: ${XDG_RUNTIME_DIR:="${${TMPDIR-/tmp}%/}/xdg-$UID"}
if ! [[ -d $XDG_RUNTIME_DIR ]]; then
  mkdir -p $XDG_RUNTIME_DIR
  chmod 0700 $XDG_RUNTIME_DIR
fi

export XDG_CONFIG_HOME XDG_CACHE_HOME XDG_DATA_HOME XDG_RUNTIME_DIR
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export DOTFILES="$HOME/dots"
export TERM=xterm-256color
export EDITOR=nvim
[[ -x "/usr/local/bin/nvim" ]] && export EDITOR="/usr/local/bin/nvim"
export VISUAL="$EDITOR"

export HISTFILE="$ZDOTDIR/.zhistory"
export HISTSIZE=10000
export SAVEHIST="$HISTSIZE"

export FZF_CTRL_T_COMMAND="fd --hidden --follow --exclude .git"
export FZF_DEFAULT_COMMAND="${FZF_CTRL_T_COMMAND} --type f"
export FZF_PREVIEW_COMMAND="bat --style=numbers,changes --wrap never \
  --color always {} || cat {} || exa --tree {}"
export FZF_DEFAULT_OPTS="--ansi --reverse --tabstop=2 --multi \
  --bind 'ctrl-a:select-all,ctrl-d:deselect-all,ctrl-p:toggle-preview,tab:toggle+up,shift-tab:toggle+down' \
  --preview '($FZF_PREVIEW_COMMAND) 2> /dev/null' \
  --preview-window down:60%:noborder"
export FZF_ALT_C_COMMAND="${FZF_CTRL_T_COMMAND} --type d . ~"
