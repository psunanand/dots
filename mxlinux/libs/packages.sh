#!/usr/bin/env bash

set -o errexit # Exit on errors
set -o errtrace # Make sure any error trap is inherited
set -o nounset # Disallow expansion of unset variables
set -o pipefail # Use last non-zero exit code in a pipeline


install_from_official_apt(){
  while read -r line
  do
    sudo apt install -y $line
  done < $(dirname ${BASH_SOURCE[0]})/db_packages.txt
}

install_from_git(){
  cd $HOME
  mkdir -p Repos && cd Repos
  while read -r line
  do
    git clone $line
  done < $(dirname ${BASH_SOURCE[0]})/nondb_packages.txt
}

symlink_local_bin(){
  sudo ln -s $(which batcat) /usr/local/bin/bat
  sudo ln -s $(which fdfind) /usr/local/bin/fd
  sudo ln -s $HOME/Repos/pyenv $HOME/.pyenv
  sudo ln -s $HOME/.pyenv/bin/pyenv /usr/local/bin/pyenv
  sudo ln -s $HOME/Repos/pyenv-virtualenv $HOME/.pyenv/plugins/pyenv-virtualenv
  sudo ln -s $HOME/Repos/fzf $HOME/.fzf
  sudo ln -s $HOME/.fzf/bin/fzf /usr/local/bin/fzf
  sudo ln -s $HOME/Repos/zsh-syntax-highlighting $HOME/.zsh-syntax-highlighting

  mkdir -p $HOME/.tmux/plugins
  sudo ln -s $HOME/Repos/tpm $HOME/.tmux/plugins/tpm

}

install_lazygit(){
  LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
  curl -Lo /tmp/lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
  tar xf /tmp/lazygit.tar.gz /tmp/lazygit
  sudo install /tmp/lazygit /usr/local/bin
}

install_delta(){
  DELTA_VERSION=$(curl -s "https://api.github.com/repos/dandavison/delta/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
  curl -Lo /tmp/delta-git "https://github.com/dandavison/delta/releases/download/${DELTA_VERSION}/git-delta-musl_${DELTA_VERSION}_amd64.deb"
  sudo dpkg -i /tmp/delta-git
}

main(){
  source "$(dirname "${BASH_SOURCE[0]}")/util.sh"
  script_init "$@"

  trap script_trap_err ERR
  trap script_trap_err EXIT

  run "Installing packages from official database" "install_from_official_apt"
  run "Installing packages from git" "install_from_git"
  run "symlink local bin" "symlink_local_bin"

  # build from sources
  run "Installing lazygit" "install_lazygit"
  run "Installing delta-git" "install_delta"
}

main "$@"
